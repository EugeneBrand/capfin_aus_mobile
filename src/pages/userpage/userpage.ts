import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../home/authenticate';
//import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HomePage } from '../home/home';
import { DetailsPage } from '../details/details';
import { AccountsPage } from '../accounts/accounts';
/*
  Generated class for the Userpage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-userpage',
  templateUrl: 'userpage.html'
})
export class UserpagePage {

  // Used to store the returned object
  cust_profile = [];
  cust_personal_details = [];
  cust_address_details = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService : AuthService) {}

  logout() {
    this.authService.logout();
    this.navCtrl.setRoot( HomePage );
  }

  nextPage() {
    this.navCtrl.push( DetailsPage );
  }

  accountPage() {
    this.navCtrl.push( AccountsPage );
  }
  /**
   * Retrieve Customer profile
   */
  getProfile() {
    Promise.resolve( this.authService.getProfile() ).then( result => {
      this.cust_profile = result;
    });
  }
  /**
   * Retrieve Customer details
   */
  getPersonalDetails() {
    Promise.resolve( this.authService.getPersonalDetails() ).then( result => {
      this.cust_personal_details = result;
    });
  }
    /**
   * Retrieve Customer details
   */
  getAddressDetails() {
    Promise.resolve( this.authService.getAddressDetails() ).then( result => {
      this.cust_address_details = result;
    });
  }
}

 
