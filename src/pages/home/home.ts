import { Component } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './authenticate';
import { UserpagePage } from '../userpage/userpage';
import { SignupPage } from '../signup/signup';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  usercreds = {
    // Temporarily hardcoded values for testing purposes => empty when done
    email : 'example1131@capfin.co.za',
    password : 'password123'
  };

  loading : any;

  constructor(public navCtrl: NavController, public authService : AuthService,
              public loadingContr : LoadingController ) {}

  login( user ) {

    this.presentLoadingDefault();

    if( this.authService.authClient( user ) ) {
      //this.loading.dismiss();
      
      this.navCtrl.setRoot( UserpagePage );
    }
    else {
      this.navCtrl.setRoot( HomePage );
    }

  }

  signup() {
    this.navCtrl.push( SignupPage );
  }
  /*
    Loading spinner 
  */
  presentLoadingDefault() {
    this.loading = this.loadingContr.create({
      spinner : 'crescent',
      content : 'Authenticating...',
      showBackdrop : true
  });
    setTimeout(() => {
      this.loading.dismiss();
    }, 2000);   
    
    this.loading.present();
  }

}
