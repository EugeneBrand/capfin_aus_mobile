import { Injectable, Pipe } from '@angular/core';

/*
  Pipe to acces nested array objects en return the key|value pairs
*/
@Pipe({
  name: 'explode-pipe'
})
@Injectable()
export class ExplodePipe {

  transform(value : any, args : any) : any {
    return Object.keys( value ).map( key => value[ key ]);
  }
}
