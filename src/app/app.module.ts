import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
// Pages
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { UserpagePage } from '../pages/userpage/userpage';
import { DetailsPage } from '../pages/details/details';
import { AccountsPage } from '../pages/accounts/accounts';
// Providers
import { AuthService } from '../pages/home/authenticate';
import { ExplodePipe } from '../pipes/explode-pipe';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    UserpagePage,
    DetailsPage,
    AccountsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    UserpagePage,
    DetailsPage,
    AccountsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, AuthService, ExplodePipe]
})
export class AppModule {}
